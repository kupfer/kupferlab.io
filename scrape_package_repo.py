#!/usr/bin/env python
import click
import json
import logging
import os

from constants import Arch, ARCHES, BASE_DISTROS
from dictscheme import DictScheme
from distro.distro import get_base_distro, get_kupfer_https, REPOSITORIES as KUPFER_REPOSITORIES
from logger import setup_logging
from config.state import config

COMPLETE_FILE = 'branch.json'
COMPLETE_FLAT_FILE = 'branch_flat.json'
PER_ARCH_FILE = 'repos.json'
PER_REPO_FILE = 'packages.json'

STATS_ARCHES = 'stats_architectures.json'
STATS_DISTROS = 'stats_distros.json'
STATS_REPOS = 'stats_repos.json'
STATS_BRANCHES = 'stats_branches.json'

DUMP_DIR = 'packages_json'
TEMPLATE_DIR = '.'

TEMPLATES = {
    "full": "_full.html.j2",
    "branch": "_branch.html.j2",
    "arch": "_arch.html.j2",
    "repo": "_repo.html.j2",
    "pkg": "_package_file.html.j2"
}


REPOS = KUPFER_REPOSITORIES + list(set(k for d in BASE_DISTROS.values() for k in d['repos'].keys()))


class RunConfig(DictScheme):
    branch: str
    verbose: bool
    tolerate_errors: bool
    output_dir: str
    template_dir: str
    overwrite: bool
    repositories: list[str]
    arches: list[Arch]
    dump_full: bool
    dump_per_arch: bool
    dump_per_distro: bool
    dump_per_repo: bool
    dump_per_pkg: bool


run_config = RunConfig()
template_cache: dict[str, str] = {}


def get_path(*path_parts):
    return os.path.join(run_config.output_dir, run_config.branch, *path_parts)


def get_template(template_file: str):
    return os.path.join(run_config.template_dir, template_file)


def copy_template(template_file: str, *dest: list[str]):
    global template_cache
    if template_file not in template_cache:
        template_cache[template_file] = '{% ' + f'extends "packages/{template_file}"' + ' %}'
    output_path = get_path(*dest)
    overwriting = False
    logfunc = logging.debug
    if os.path.exists(output_path):
        if run_config.overwrite:
            overwriting = True
            logfunc = logging.warning
        else:
            logging.warning(f"Path {output_path} already exists, not overwriting from {template_file}")
            return
    logfunc(f'{"OVERWRITING" if overwriting else "Deploying"} "{template_file}" at {output_path}')
    with open(output_path, 'w') as fd:
        fd.write(template_cache[template_file])


@click.group('main')
@click.option('-v', '--verbose', help='verbose output', is_flag=True)
@click.option('-E', '--tolerate-errors', is_flag=True)
@click.option('-o', '--output-dir', default=DUMP_DIR, type=click.Path(exists=False))
@click.option('-t', '--template-dir',  default=TEMPLATE_DIR, type=click.Path(exists=True))
@click.option('-O', '--overwrite', help='Overwrite existing files', is_flag=True)
@click.option('--arches', multiple=True, type=click.Choice(ARCHES))
@click.option('--branch', type=click.Choice(['main', 'dev']))
@click.option('--repositories', multiple=True, type=click.Choice(REPOS))
@click.option('--dump-full/--no-dump-full', default=True, is_flag=True)
@click.option('--dump-per-arch/--no-dump-per-arch', default=True, is_flag=True)
@click.option('--dump-per-distro/--no-dump-per-distro', default=False, is_flag=True)
@click.option('--dump-per-repo/--no-dump-per-repo', default=True, is_flag=True)
@click.option('--dump-per-pkg/--no-dump-per-pkg', default=False, is_flag=True)
def cmd_main(
    verbose: bool = True,
    tolerate_errors: bool = False,
    output_dir: str = DUMP_DIR,
    template_dir: str = TEMPLATE_DIR,
    overwrite: bool = False,
    repositories: list[str] = REPOS,
    arches: list[Arch] = ARCHES,
    branch: str = None,
    dump_full: bool = True,
    dump_per_arch: bool = True,
    dump_per_distro: bool = False,
    dump_per_repo: bool = True,
    dump_per_pkg: bool = False,
):
    global run_config

    setup_logging(verbose=verbose)
    config.try_load_file()

    run_config.verbose = verbose
    run_config.tolerate_errors = tolerate_errors
    run_config.output_dir = output_dir
    run_config.template_dir = template_dir
    run_config.overwrite = overwrite
    run_config.repositories = repositories or REPOS
    run_config.arches = arches or ARCHES
    run_config.branch = branch or config.file.pacman.repo_branch
    run_config.dump_full = dump_full
    run_config.dump_per_arch = dump_per_arch
    run_config.dump_per_distro = dump_per_distro
    run_config.dump_per_repo = dump_per_repo
    run_config.dump_per_pkg = dump_per_pkg


@cmd_main.command('genhtml')
def cmd_gen_html(
    _repos: dict[Arch, dict] | None = None
):
    global run_config
    data: dict[Arch, dict] = _repos or None
    if not data:
        with open(get_path(COMPLETE_FILE)) as fd:
            jdata = json.load(fd)
            data = jdata
    if run_config.dump_full:
        copy_template(TEMPLATES["branch"], "index.html")
        copy_template(TEMPLATES["full"], "..", "index.html")
    for arch in run_config.arches:
        if arch not in data:
            logging.warning(f"Architecture {arch} not found in data!")
            continue
        if run_config.dump_per_arch:
            copy_template(TEMPLATES["arch"], arch, "index.html")
        repos = data[arch]
        for repo_name, repo in repos.items():
            if repo_name not in run_config.repositories:
                logging.debug(f'Data contains repository "{repo_name}" which is not in repo filters. Skipping.')
                continue
            if run_config.dump_per_repo:
                copy_template(TEMPLATES["repo"], arch, repo_name, "index.html")
            if not run_config.dump_per_pkg:
                continue
            for package_name in repo:
                for d in [repo_name, 'packages']:
                    copy_template(TEMPLATES["pkg"], arch, d, f"{package_name}.html")


def dump_json_file(contents: dict | list, *path):
    full_path = get_path(*path)
    logging.debug(f'Dumping JSON file "{full_path}"')
    os.makedirs(os.path.dirname(full_path), exist_ok=True)
    with open(full_path, 'w') as fd:
        json.dump(contents, fd)


def update_json_entries(entries: dict, *path):
    full_path = get_path(*path)
    exists = os.path.exists(full_path)
    logging.debug(f'Reading JSON file "{full_path}"' +
                  (": Skipping, file doesn't exist" if not exists else ""))
    contents = {}
    if exists:
        with open(full_path, 'r') as fd:
            contents = json.load(fd)
        if not isinstance(contents, dict):
            logging.warning(f"Ignoring {full_path} contents as it's {type(contents)!r} instead of a dict")
            contents = {}
    contents.update(entries)
    dump_json_file(contents, *path)


@cmd_main.command('dump')
@click.option('-k/-K', '--include-kupfer/--no-include-kupfer', default=True, is_flag=True)
@click.option('-b/-B', '--include-base-distro/--no-include-base-distro', default=False, is_flag=True)
@click.option('--echo', help="Print the full json dump to stdout", default=False, is_flag=True)
@click.option('-g', '--genhtml', help='Generate html stubs', is_flag=True)
@click.pass_context
def cmd_dump(
    ctx: click.Context,
    include_kupfer: bool = True,
    include_base_distro: bool = False,
    echo: bool = False,
    genhtml: bool = False,
):
    global run_config

    arches = run_config.arches
    repositories = run_config.repositories
    results: dict[str, dict] = {}
    stats = {"packages": 0, "arches": {}}
    for _arch in arches:
        stats["arches"][_arch] = {"distros": {}, "repos": {}, "packages": 0}
        for distro_name, distro in [
            (name, values[0])
            for name, values
            in {
                "Kupfer": (get_kupfer_https, include_kupfer),
                "Arch Linux": (get_base_distro, include_base_distro),
            }.items()
            if values[-1]
        ]:
            d = distro(arch=_arch, scan=False)
            stats["arches"][_arch]["distros"][distro_name] = {"repos": list(d.repos.keys()), "packages": 0}
            repos: dict[str, dict[str, dict]] = {}
            for name, repo_obj in d.repos.items():
                if not (name in repositories and repo_obj.scan(allow_failure=run_config.tolerate_errors)):
                    continue
                repo = {
                    n: p._desc | {  # type: ignore[operator]
                        'distro': distro_name,
                        'repo_arch': _arch,
                        'repo_branch': run_config.branch,
                        'repo_name': name,
                    }
                    for n, p
                    in repo_obj.packages.items()
                }
                repos[name] = repo
                results[_arch] = results.get(_arch, {}) | repos

                if run_config.dump_per_repo:
                    dump_json_file(list(repo.values()), _arch, name, PER_REPO_FILE)

                if not run_config.dump_per_pkg:
                    continue
                for package_name, package in repo.items():
                    filename = f'{package_name}.json'
                    filepath = f'{name}/{filename}'
                    symlink_source = f'../{filepath}'
                    symlink_location = get_path(os.path.join(_arch, 'packages', filename))
                    dump_json_file(package, _arch, filepath)
                    os.makedirs(os.path.dirname(symlink_location), exist_ok=True)
                    if os.path.exists(symlink_location):
                        logging.warning(f'Symlink for {name}/{filename} exists already at {symlink_location}')
                        continue
                    logging.debug(f'Symlinking: "{symlink_location}" -> "{symlink_source}"')
                    os.symlink(symlink_source, symlink_location)

                repo_count = len(repo)
                stats["arches"][_arch]["repos"][name] = repo_count
                stats["arches"][_arch]["distros"][distro_name]["packages"] += repo_count
                stats["arches"][_arch]["packages"] += repo_count
                stats["packages"] += repo_count
                update_json_entries({name: stats["arches"][_arch]["repos"][name]}, _arch, STATS_REPOS)
            update_json_entries({distro_name: stats["arches"][_arch]["distros"][distro_name]}, _arch, STATS_DISTROS)
            if run_config.dump_per_distro:
                dump_json_file(repos, _arch, f"{distro_name.replace(' ', '').lower()}.json")

        if run_config.dump_per_arch:
            arch_pkgs = [p for r in results.get(_arch, {}).values() for p in r.values()]
            dump_json_file(arch_pkgs, _arch, PER_ARCH_FILE)
        update_json_entries({_arch: stats["arches"][_arch]}, STATS_ARCHES)
    update_json_entries({run_config.branch: stats}, '..', STATS_BRANCHES)
    if run_config.dump_full:
        dump_json_file(results, COMPLETE_FILE)
        dump_json_file([p for a in results.values() for r in a.values() for p in r.values()], COMPLETE_FLAT_FILE)
    if echo:
        print()
        print(json.dumps(results, indent=4))

    if genhtml:
        ctx.invoke(cmd_gen_html, _repos=results)


if __name__ == '__main__':
    cmd_main()
