---
title: Leadership Changes
date: 2022-05-25 13:30:00
author: Prawn
---

> **Kupfer is dead; long live Kupfer!**


Since Kupfer was founded by Kate in August 2021, we've made a number of achievements, including porting postmarketOS's `crossdirect` tooling.

Sadly, last month Kate announced that they'd be stepping down from Kupfer:

> @room hey i wanted to give an update on the state of Kupfer, I've decided to step down and leave the project. if anyone wants to continue this (probably @Prawn), i'd be happy to handover ownership permissions.
> <br>
> i started this project originally in october 2020 when i hacked arch onto my poco f1. i learned a ton about arch, linux, bootloaders, android and other stuff. i certainly had a lot of fun.
> <br>
> about a year ago i realized the approach wouldn't scale so decided to create kupfer. more people got interested, you know the story...
> <br>
> since then i've gotten a software engineering job and i haven't been able to make time to work on Kupfer (or my other personal projects).
> <br>
> i've thought a lot about our approach and at least personally i don't think it makes sense.
> we don't need more distros to run on our phones, we need to work on upstreaming support for our devices so that they can support more distros (see [https://drewdevault.com/2022/01/18/Pine64s-weird-priorities.html](https://drewdevault.com/2022/01/18/Pine64s-weird-priorities.html)). additionally, postmarketos is a really great project and there are so many talented people working on it. it's impossible for us to match that kind of development. i don't want to say arch is bad (btw i use arch on my workstation), but just reimplementing everything pmos does is really just a waste of energy. imagine in the future you could just put any arm64 distro on your phone and it would just work ootb. i think to reach this goal we shouldn't devide the community into more distros, but join efforts (in this case through pmos for example).
> <br>
> i think the only way kupfer would really stand out is by shipping a lot of -git packages that get updated daily, so people can run really bleeding edge software and get a bunch of cool stuff earlier than on other distros. otherwise i don't really see a compelling point about running arch on a phone. distro ideology is dead.
> <br>
> p.s.: there was no conflict with anyone, but i saw this coming in the last six months. sorry to anyone who is disappointed.

As you can see from reading this, I - Prawn - have indeed decided to step up and carry on for as long as I have the time and energy.

Say hello to your new benevolent dictator I suppose.

I'm not sure I agree with Kate's vision of running latest git commits everywhere, at least for the time being.

Anyway, that's it for now. There is [a second blogpost](2022-05-26_future_of_kupfer.html) detailing the near future of Kupfer.

**P.S.:** In memory of old Kupfer, or as I've recently taken to calling it, *Kupfer Classic*, [here's the old logo](/assets/img/content/kupfer_logo_old.png), grabbed from Matrix.
I will be going through Gitlab and Matrix in the next few days and replacing these with the new logo.
