---
title: v0.1.4: Design Update
date: 2022-10-15 01:33:07
author: Prawn
---

Hello!

in order to boost our release stats, I'm publishing a super minor update. :^)

Semantically it should be v0.1.3.1 but I'm not gonna split hairs like that while v0.2 is getting closer and closer.

This update mostly brings the removal of python2 and some small design updates to this very website and the [KBS docs](https://kupfer.gitlab.io/kupferbootstrap).

This Website/Blog has gotten an update to its [default dark theme](/assets/img/content/2022-10_design-old.png), namely a darker background and copper accent colours, and a toggle for a light mode (sunglasses recommended) on the top right.<br>
Also a [one line fix](https://stackoverflow.com/a/30459783) to finally enable responsive viewing of this page on mobile. (ironic, isn't it?)

The dark mode mechanism has been yoinked and slightly modified from the [furo sphinx theme](https://github.com/pradyunsg/furo) we use for the kupferbootstrap docs.


## Kupfer v0.1.4

Not a lot of changes.

### kupferbootstrap

[kupferbootstrap v0.1.4](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.1.4)

- [**docs: switch to copper accent colours**](https://gitlab.com/kupfer/kupferbootstrap/-/commit/e2e1ba14592bdedc92b1c9c5712da7010df0ff0e)


### PKGBUILDS

[pkgbuilds v0.1.4](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.1.4)


- [**remove outdated python2 dependency from mkbootimg-git**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/d1097868c504ccca5cf3df78ffc2209e7ec77ea2):
  <br>Python2 finally got dropped from Arch Linux, turns out the package is python3 compatible by now anyway.
