---
title: v0.2.0-rc1: Versioned Kupferbootstrap docs
date: 2022-12-20
author: Prawn
---

Hi there!

I've tagged [Kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc1) and [PKGBUILDs](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc1) `dev` as `v0.2.0-rc1`!

If you're on `dev` and have missed the [rc0 release](2022-11-12_v0-2-0rc0_merged.html), please go read up on the [migration instructions](2022-11-12_v0-2-0rc0_merged.html#migration-instructions)

## What's new in v0.2-rc1

### Kupferbootstrap

[Kupferbootstrap v0.2.0-rc1](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc1)

- The [KBS docs](https://kupfer.gitlab.io/kupferbootstrap/) are now built for `main`, `dev` and all version tags that have docs. A version selector is now available.
- I've reworked (read: hopefully finished) the .SRCINFO caching mechanisms. The cache files no longer need to be committed to PKGBUILDs.git, and both the `packages init` and `packages update` commands have gained a [`--init-caches` flag](https://kupfer.gitlab.io/kupferbootstrap/v0.2.0-rc1/cli/packages/#cmdoption-kupferbootstrap-packages-init-init-caches) to quickly download the caches from the binary repos.
- [Use a chroot for enabling binfmt](75d6ea8c3c5d3c7b8a26f998df6eb48952065a47): This should help cross-distro compat and get us closer towards a chroot-wrapper
- [Don't use `makepkg --holdver` anymore](https://gitlab.com/kupfer/kupferbootstrap/-/commit/6fa717ce64d282ece223f0c47e917e465beb3a19): it causes nasty edge cases for dubious gains, especially now that we have file caching.

### PKGBUILDs

[PKGBUILDs v0.2.0-rc1](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc1)

- [**Phosh and friends**: 0.22](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/383f8ceadb720dfdcf070e7e3ebd3d8cd8bf0204): The terminal app `kgx` has landed in Arch Linux as `gnome-console`, don't be afraid when pacman prompts you to remove `kgx` due to [a (purposely existing) conflict](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/d56eaad0f91d7976ef7655593917fa015da9f1c9) with our gnome-minimal package.
- [**vconsole-hidpi-config**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/7068a6ec4b8b4a7b9fba73f8b9cf451b772fb7da): Small package to configure a bigger font size for TTYs, useful on high DPI devices like the SDM845 phones. Thanks Syboxez!
- [**firefox-mobile-config**: 3.1.0](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/ed946c2e207692ea784d57f27b301156c5c881a6)


## Coming up:

- `feedbackd` update, vibration and LED feedback on SDM845! (at least tested on enchilada)
- SDM845 call audio (there's an issue with loading the new alsa ucm config)
- Disk Encryption
    - finally merge the `initsquared` shim initramfs package
    - add some OSK hook. which OSK is still tbd
- Switch to postmarketOS' [boot-deploy](https://gitlab.com/postmarketOS/boot-deploy) for building android boot.img (no change for users, just replacing homebrewed utils with pmos upstreams)
