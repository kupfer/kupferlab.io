---
author: Prawn
date: 2022-05-26 18:00
title: <code>reboot()</code> - The Future Of Kupfer
---


## Quick Recap

Since [taking over from Kate](2022-05-25_leadership-changes.html), we've made some progress on a couple of fronts:

1. I published most of my `kupferbootstrap` overhaul to the `dev` branch. This includes (amongst others)
    - proper crosscompilation and `crossdirect` support (`rustc` is still broken in qemu though)
    - a bunch of documentation and new flags for the CLI commands
    - configuration file with device profiles
    - chroot handling as a class
1. Updated a bunch of packages, most notably phosh and the kernels. (Thanks Syboxez!)
1. Initial modem support (data only, not stable for me yet) on sdm845!
1. We've reactivated the CI for package build testing and -publishing.
1. We've added a [`dev`](https://gitlab.com/kupfer/packages/prebuilts/-/tree/dev) branch to prebuilts.git
1. I've created this website and added the blog feature. You're looking at it! (also featuring a new logo I threw together)
1. **With this blog post, we're celebrating the release of version v0.1.0 of [`kupferbootstrap`](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.1.0) and [`pkgbuilds`](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.1.0)!\***

## Whispers of future times

Now to the good stuff:  our plans for the short- and long-term future:

1. Meta-packages for devices and flavours (currently in kupferbootstrap's `constants.py`)
1. Init Shim -> Full Disk Encryption
1. Start building images in CI
1. Move the binary package repos out of git onto proper hosting
1. Package signing
1. Proper `kupferbootstrap` support for mixing locally built packages with our Kupfer mirrors, auto-download, etc.
1. More commands for handling PKGBUILDs: `packages checksum`, `packages bump {pkgver,pkgrel}`, etc.
1. Wait for rustc to work again (`llvm14`?) and/or add native aarch64 builders to the CI to work around the problem
   (this is why we currently don't have squeekboard)
1. Long-term goal: see if we can get meson crosscompiling working; in theory we can then use the native `/bin/rustc`.
1. Supersede the docker wrapper with chroot-wrapping, use sudo, etc.
1. Further refactor kupferbootstrap (code-cleanups)
1. More packages (flavours!)
1. More devices
1. More contributors
1. More tags
1. More blog entries
1. More free time
1. (Port `ufs-qcom.c` to u-boot)

Wait, this isn't my personal wishlist?! ... Sorry, I must've drifted off.

## Development strategy

Kupferbootstrap v0.1.0 has been tagged from the `dev` branch today and pushed to `main` as well.

The same actions were applied to our `pkgbuilds` and `prebuilts`.

From now on, current development will happen on the `dev` branch, where occasional force-pushes might happen when I screw up.

Work from `dev` will be tagged and released to `main` at certain milestones, hopefully somewhat frequently.


### Timeline

Now I initially didn't want to make any estimates. I've settled on a compromise.

I plan to target those first 5 items for `v0.2`, all of them if possible.

Mixing local and remote repos and the `packages checksum` and `bump` commands should be in `v0.3`.

Releases will be done when they're done, but for what it's worth: I don't want many, many months to pass between them, if I can help it.
That's as concrete as I'll get for now.

Community makes FOSS go brrr, so send patches pls 😉 We're always grateful for contributions.

<hr class="border-white"/>


\* I've also tagged these repos' old `main` and `dev` branch states as `v0.0.1` and `v0.0.2` respectively for the internet historians.